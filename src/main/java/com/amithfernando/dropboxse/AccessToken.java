package com.amithfernando.dropboxse;

import com.dropbox.core.DbxAppInfo;
import com.dropbox.core.DbxAuthFinish;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.DbxWebAuthNoRedirect;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Locale;

/**
 *
 * @author Amith Fernando
 */
public class AccessToken {

    public static final String APP_KEY = "APP_KEY";
    public static final String APP_SECRET = "APP_SECRET";

    public static void main(String[] args) {
        try {
            // Get your app key and secret from the Dropbox developers website.
            DbxAppInfo appInfo = new DbxAppInfo(APP_KEY, APP_SECRET);

            DbxRequestConfig config = new DbxRequestConfig("JavaTutorial/1.0", Locale.getDefault().toString());
            DbxWebAuthNoRedirect webAuth = new DbxWebAuthNoRedirect(config, appInfo);

            // Have the user sign in and authorize your app.
            String authorizeUrl = webAuth.start();
            System.out.println("1. Go to: " + authorizeUrl);
            System.out.println("2. Click \"Allow\" (you might have to log in first)");
            System.out.println("3. Copy the authorization code.");
            String code = new BufferedReader(new InputStreamReader(System.in)).readLine().trim();

            DbxAuthFinish authFinish = webAuth.finish(code);
            String accessToken = authFinish.accessToken;
            System.out.println("Access Token  " + accessToken);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
